public class TestFlip extends Activity implements OnGestureListener {  

private ViewFlipper flipper;  

private GestureDetector detector;  

/** Called when the activity is first created. */  
@Override  
public void onCreate(Bundle savedInstanceState) {  
super.onCreate(savedInstanceState);  
setContentView(R.layout.main);  

detector = new GestureDetector(this);  
flipper = (ViewFlipper) this.findViewById(R.id.ViewFlipper01);  
flipper.addView(addView(R.layout.layout1));  
flipper.addView(addView(R.layout.layout2));  
flipper.addView(addView(R.layout.layout3));  
flipper.addView(addView(R.layout.layout4));  
}  

private View addView(int layout) {  
LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
View view = inflater.inflate(layout, null);  
return view;  
}  

@Override  
public boolean onTouchEvent(MotionEvent event) {  
return this.detector.onTouchEvent(event);  
}  

@Override  
public boolean onDown(MotionEvent e) {  
// TODO Auto-generated method stub   
return false;  
}  

@Override  
public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,  
float velocityY) {  
if (e1.getX() - e2.getX() > 120) {  
this.flipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.push_left_in));  
this.flipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.push_left_out));  
this.flipper.showNext();  
return true;  
} else if (e1.getX() - e2.getX() < -120) {  
this.flipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.push_right_in));  
this.flipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.push_right_out));  
this.flipper.showPrevious();  
return true;  
}  
return false;  
}  

@Override  
public void onLongPress(MotionEvent e) {  
// TODO Auto-generated method stub   

}  

@Override  
public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,  
float distanceY) {  
// TODO Auto-generated method stub   
return false;  
}  

@Override  
public void onShowPress(MotionEvent e) {  
// TODO Auto-generated method stub   

}  

@Override  
public boolean onSingleTapUp(MotionEvent e) {  
// TODO Auto-generated method stub   
return false;  
}  
}  